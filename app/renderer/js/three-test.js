// var spotlightY;

// once everything is loaded, we run our Three.js stuff.
function init() {

    // create a scene, that will hold all our elements such as objects, cameras and lights.
    var scene = new THREE.Scene();
    scene.background = new THREE.Color( 0x000000 );

    // create a camera, which defines where we're looking at.
    var camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.1, 1000);
    // create a render and set the size
    var renderer = new THREE.WebGLRenderer({antialias: true});

    // renderer.setClearColor(new THREE.Color(0x000, 1.0));

    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.shadowMap.enabled = true;

    var controls = new THREE.OrbitControls( camera, renderer.domElement );

    // position and point the camera to the center of the scene
    camera.position.x = 0;
    camera.position.y = 0;
    camera.position.z = 40;

    // camera.lookAt({x:0, y:0, x:0});

    // add some lights
    var spotLight1 = new THREE.SpotLight(0xffffff, 0.3, 0, Math.PI/4);
    spotLight1.position.set(-20, 20, 5);
    scene.add(spotLight1);

    var spotLight1b = new THREE.SpotLight(0xffffff, 0.25, 0, Math.PI/4);
    spotLight1b.position.set(-20, -15, 5);
    scene.add(spotLight1b);

    var spotLight2 = new THREE.SpotLight(0xffffff, 0.6, 0, Math.PI/4);
    spotLight2.position.set(-5, 30, 30);
    spotLight2.castShadow = true;
    spotLight2.shadow = new THREE.LightShadow( new THREE.PerspectiveCamera( 30, 1, 10, 50 ) );

    scene.add(spotLight2);

    var spotLight3 = new THREE.SpotLight(0xffffff, 3, 0, Math.Pi/4);
    spotLight3.position.set(20, 20, -10);
    scene.add(spotLight3);


    // add the output of the renderer to the html element
    document.getElementById("WebGL-output").appendChild(renderer.domElement);
    render();

    // load the model
    var obj;
    var head;
    var frontP;

    loadModel();
    function loadModel() {
        var textLoader = new THREE.TextureLoader();

        var diffuseTexture = textLoader.load("./resources/bay-21.png");
        var bumpTexture = textLoader.load("./resources/bay-21-bump.png");
        var emissiveTexture = textLoader.load("./resources/bay-21-emission.png");
        var specularTexture = textLoader.load("./resources/bay-21-specular.png");

        var loader = new THREE.OBJLoader();
        loader.load("./resources/landing-platform-2.obj", function(model) {
            model.children.forEach(function(child) {
                // child.scale.set(0.0265810822, 0.0265810822, 0.0265810822);
                var material = child.material;

                // basic texture
                material.map = diffuseTexture;

                // bumps
                material.bumpMap = bumpTexture;
                material.bumpScale = 0.3;

                // glow
                material.emissive = new THREE.Color(0xffffff);
                material.emissiveMap = emissiveTexture;

                // specular
                material.specularMap = specularTexture;

                child.receiveShadow = true;
                child.castShadow = true;

                if (child.name === "Head") head = child;
                if (child.name === "Front_Projector") frontP = child;
            });

            model.scale.x = 0.15;
            model.scale.y = 0.15;
            model.scale.z = 0.15;

            model.position.y = -10;
            obj = model;
            scene.add(obj);
        }, function ( xhr ) {
                console.log( (xhr.loaded / xhr.total * 100) + '% loaded' );
                
                
            })
    }

    var rotMax = 0.5 * Math.PI;
    var rotMin = -0.5 * Math.PI;

    var spotlightY = 10;
    var direction = -0.3;

    console.log(direction, spotlightY);

    function render() {
        // render using requestAnimationFrame
        
        renderer.render(scene, camera);

        spotLight1.position.y = spotlightY;
        spotlightY += direction;

        if (spotlightY < -30 || spotlightY > 10) {
            console.log("reset ", spotlightY);
            direction *= -1;
        } else {
            // console.log("noreset", spotlightY);
        }
        requestAnimationFrame(render);
    }
}

window.onload = init;